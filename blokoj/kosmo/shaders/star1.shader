shader_type spatial;
render_mode unshaded;
// USING https://www.shadertoy.com/view/XtBXDw for 3dclouds and https://www.shadertoy.com/view/4dsXWn for 2d clouds
uniform float iTime;
uniform float noise_freq;
uniform sampler2D Noise;
uniform vec4 sun_color: hint_color;

lowp float noise( in vec3 pos )
{
    pos*=0.01;
	lowp float  z = pos.z*256.0;
	lowp vec2 offz = vec2(0.317,0.123);
	lowp vec2 uv = pos.xy + offz*floor(z); 
	return mix(textureLod( Noise, uv ,0.0).x,textureLod( Noise, uv+offz ,0.0).x,fract(z));
}

lowp float get_noise(vec3 p, float FBM_FREQ)
{
	lowp float
	t  = 0.51749673 * noise(p); p *= FBM_FREQ;
	t += 0.25584929 * noise(p); p *= FBM_FREQ;
	t += 0.12527603 * noise(p); p *= FBM_FREQ;
	t += 0.06255931 * noise(p);
	return t;
}

void fragment()
{
	lowp vec4 space=vec4(0.0);
	lowp vec2 p = UV - 0.5; 
	float dist = length(p);
	lowp float sun_amount; lowp float alpha;
	sun_amount = 1.0-smoothstep(0.38,0.4,dist); 
	sun_amount += 1.0-smoothstep(0.3,0.5,dist);
	sun_amount = clamp(sun_amount,0.0,1.0);
	alpha = sun_amount;
	sun_amount *= clamp(get_noise(vec3(UV.x,UV.y,1.0)+iTime,noise_freq)+0.3,0.5,1.0);
	space = sun_color*sun_amount;
	space.rgb =mix(vec3(0.0), space.rgb,alpha);
	ALBEDO=space.rgb;
	ALPHA = alpha;
}