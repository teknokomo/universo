extends Node
# модуль создания маршрутов
#  - движения
#  - стрельбы


# анализируем (разбираем) проект с сервера и распределяем по соответствующим направлениям
# projektoj - массив проектов, пришедший с сервера
# sxipo - указатель на корабль, которому делаем
# fenestro_itinero - форма маршрута движения
func analizo_projekto(projektoj, fenestro_itinero):
	#проходим по списку проектов и распределяем по назначениям
	var one_movado = false # признак, что проект движения уже был и следующий закрываем
	var i_projektoj = 0
	fenestro_itinero.projekto_itineroj_uuid=''
	for prj in projektoj:
		for kategorio in prj['node']['kategorio']['edges']:
#			выбираем проекты с категорией движения
			if kategorio['node']['objId'] == Net.kategorio_movado: # это проект движения
				if one_movado:
#					if 'send_fermi_projekto' in sxipo: # альтернативные записи
					if fenestro_itinero.get('send_fermi_projekto'):
						print('закрываем проект, т.к. проект движения уже есть!!! должно быть на стороне сервера')
						fenestro_itinero.send_fermi_projekto(prj['node']['uuid'])
						# удаляем данный проект из списка
						projektoj.remove(i_projektoj)
				else:
					#  проверяем, что есть действующие задачи движения, если нет - закрываем проект
					if len(prj['node']['tasko']['edges'])==0:
						if fenestro_itinero.get('send_fermi_projekto'):
							print('закрываем проект, т.к. нет задач!!! должно быть на стороне сервера')
							fenestro_itinero.send_fermi_projekto(prj['node']['uuid'])
							# закрываем проект, т.к. нет задач!!! должно быть на стороне сервера
					else:
						# очищаем маршрут
						fenestro_itinero.malplenigi_itinero()
						#заполняем маршрут
						# var pozicio = 0
						for tasko in prj['node']['tasko']['edges']:
							fenestro_itinero.sxangxi_itinero(prj['node'], tasko['node'])


							#задачу, которая "В работе" ставим первой
							# if tasko['node']['statuso']['objId']==Net.statuso_laboranta \
							# 		or tasko['node']['statuso']['objId']==Net.status_pauzo: # или задачу, которая на "Паузе"
							# 	fenestro_itinero.add_itinero(
							# 		tasko['node']['uuid'],
							# 		'',
							# 		'координаты в космосе',
							# 		tasko['node']['finKoordinatoX'],
							# 		tasko['node']['finKoordinatoY'],
							# 		tasko['node']['finKoordinatoZ'],
							# 		Transform(Basis.IDENTITY, Vector3(tasko['node']['finKoordinatoX'],
							# 			tasko['node']['finKoordinatoY'],
							# 			tasko['node']['finKoordinatoZ'])),
							# 		$ship.translation.distance_to(Vector3(
							# 			tasko['node']['finKoordinatoX'],
							# 			tasko['node']['finKoordinatoY'],
							# 			tasko['node']['finKoordinatoZ']),
							# 		Net.kategorio_movado
							# 	))
							# 	if tasko['node']['statuso']['objId']==Net.status_pauzo:#задачу, которая "В работе" ставим первой
							# 		Global.fenestro_itinero.itinero_pause = true
							# 	break;
						# for tasko in prj['node']['tasko']['edges']:
						# 	if tasko['node']['statuso']['objId']==Net.statuso_nova:# добавляем остальные задачи
						# 		Global.fenestro_itinero.add_itinero(
						# 			tasko['node']['uuid'],
						# 			'',
						# 			'координаты в космосе',
						# 			tasko['node']['finKoordinatoX'],
						# 			tasko['node']['finKoordinatoY'],
						# 			tasko['node']['finKoordinatoZ'],
						# 			Transform(Basis.IDENTITY, Vector3(tasko['node']['finKoordinatoX'],
						# 				tasko['node']['finKoordinatoY'],
						# 				tasko['node']['finKoordinatoZ'])),
						# 			$ship.translation.distance_to(Vector3(
						# 				tasko['node']['finKoordinatoX'],
						# 				tasko['node']['finKoordinatoY'],
						# 				tasko['node']['finKoordinatoZ']),
						# 				Net.kategorio_movado
						# 		))
						# if len(Global.fenestro_itinero.itineroj)>0:# есть задачи на полёт, устанавливаем uuid проекта
						# 	Global.fenestro_itinero.projekto_itineroj_uuid=prj['node']['uuid']
						# 	#отправляем корабль по координатам
						# 	Global.fenestro_itinero.itinero_pause = false
						# 	#запускаем таймер 
						# 	$timer.start()
						# 	Global.fenestro_itinero.get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero").disabled=true
						# 	Global.fenestro_itinero.get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next").disabled=false
						# 	Global.fenestro_itinero.get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin").disabled=false
						# 	Global.fenestro_itinero.get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear").disabled=false
						# 	one_movado = true
						# else: # закрываемп проект, т.к. в нем нет действующих задач
						# 	pass
			elif kategorio['node']['objId'] == Net.projekto_kategorio_pafado: # это проект стрельбы
				# закрываем проект
				# print('=== закрываем проект v3=',prj['node']['uuid'],'==',fenestro_itinero.name)
				if fenestro_itinero == Global.fenestro_itinero: # если это проект управляемого объекта
					# print('закрываем проект, т.к. проекта стрельбы при входе быть не должно')
					Net.send_json(Queries.finado_projeko(prj['node']['uuid']))
					# удаляем данный проект из списка
					projektoj.remove(i_projektoj)
				# else:
				# 	print('==uuid=',fenestro_itinero.uuid)
			# else:
			# 	print('=== категория = ',kategorio['node']['objId'])
		i_projektoj += 1
