extends Node


# ТОЛЬКО ДЛЯ СЕРВЕРА
# повреждение от выстрела / изменение целостности объекта 
# астероиду убавляем и объём - указываем новые объёмы
func integreco_shanghi(uuid, integreco, volumenoEkstera, 
		volumenoInterna, volumenoStokado, id=0):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var volumeno = ''
	var per_volumeno = ''
	if volumenoInterna:
		volumeno = "volumenoInterna:$volumenoInterna, volumenoEkstera:$volumenoEkstera, volumenoStokado:$volumenoStokado"
		per_volumeno = "$volumenoInterna:Float, $volumenoEkstera:Float, $volumenoStokado:Float"

	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($uuid:UUID, '+
		' $integreco:Int, '+
		per_volumeno +
		')'+
		'{ redaktuUniversoObjektoIntegreco (uuid: $uuid,  '+
		volumeno +
		' integreco:$integreco) { status '+
		' message universoObjektoj { uuid } } '+
		'}',
		'variables': {"uuid": uuid, "integreco":integreco, "volumenoInterna":volumenoInterna, 
		"volumenoEkstera":volumenoEkstera, "volumenoStokado":volumenoStokado } }})
	# if Global.logs:
	# 	print('===integreco_shanghi=',query)
	return query


# изменение объемов объекта
func volumeno_shanghi(uuid, volumenoInterna, volumenoEkstera, volumenoStokado):
	var id = Net.get_current_query_id()
	Net.net_id_clear.push_back(id)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation  ($uuid:UUID, $volumenoInterna:Float, '+
		" $volumenoEkstera:Float, $volumenoStokado:Float) "+
		" { redaktuUniversoObjekto (uuid:$uuid, volumenoInterna:$volumenoInterna, "+
		" volumenoEkstera:$volumenoEkstera, volumenoStokado:$volumenoStokado) "+
		' {status message universoObjektoj{ uuid } } }',
		'variables': {"uuid": uuid, "volumenoInterna":volumenoInterna, 
		"volumenoEkstera":volumenoEkstera, "volumenoStokado":volumenoStokado } }})
	if Global.logs:
		print('===volumeno_shanghi=',query)
	return query
	

# создание объекта
func krei_objekto(nomo, priskribo, posedantoTipoId, posedantoStatusoId, posedantoUzantoSiriusoUzantoId,
		ligiloTipoId, ligiloPosedantoUuid, resursoId, volumenoInterna, 
		volumenoEkstera, volumenoStokado, id):
	# var id = Net.get_current_query_id()
	Net.net_id_clear.push_back(id)
	var query = JSON.print({
		'type': 'start',
		'id': '%s' % id,
		'payload':{ 'query': 'mutation  ($nomo:String, $priskribo:String, '+
		' $realecoId:Int, $posedantoTipoId:Int, $posedantoStatusoId:Int, '+
		' $posedantoUzantoSiriusoUzantoId:Int, $ligiloTipoId:Int, '+
		' $resursoId:Int, $ligiloPosedantoUuid:String, '+
		' $volumenoInterna:Float, $volumenoEkstera:Float, '+
		' $volumenoStokado:Float) '+
		'{ redaktuUniversoObjekto ( volumenoInterna:$volumenoInterna, '+
		" volumenoEkstera:$volumenoEkstera, volumenoStokado:$volumenoStokado, "+
		'  nomo:$nomo, priskribo:$priskribo, '+
		'  publikigo:true, '+
		'  resursoId:$resursoId, '+
		'  kuboNull:true, '+
		'  posedantoParto:100, '+
		'  realecoId:$realecoId, '+
		'  posedantoTipoId:$posedantoTipoId, '+
		'  posedantoStatusoId:$posedantoStatusoId, '+
		'  posedantoUzantoSiriusoUzantoId:$posedantoUzantoSiriusoUzantoId, '+
		'  ligiloTipoId:$ligiloTipoId, '+
		'  ligiloPosedantoUuid:$ligiloPosedantoUuid '+
		') { status  message universoObjektoj { uuid } } } ',
		'variables': {"nomo": nomo, "priskribo":priskribo, 
		"realecoId":Global.realeco, "posedantoTipoId":posedantoTipoId,
		"posedantoStatusoId":posedantoStatusoId, 
		"posedantoUzantoSiriusoUzantoId":posedantoUzantoSiriusoUzantoId,
		"ligiloTipoId":ligiloTipoId, "ligiloPosedantoUuid":ligiloPosedantoUuid,
		"resursoId":resursoId, "volumenoInterna":volumenoInterna, 
		"volumenoEkstera":volumenoEkstera, "volumenoStokado":volumenoStokado } }})
	# if Global.logs:
	# 	print('===krei_objekto=',query)
	return query



